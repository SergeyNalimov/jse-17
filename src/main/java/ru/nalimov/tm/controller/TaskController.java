package ru.nalimov.tm.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nalimov.tm.Application;
import ru.nalimov.tm.entity.Task;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.exceptions.TaskNotFoundException;
import ru.nalimov.tm.service.ProjectService;
import ru.nalimov.tm.service.ProjectTaskService;
import ru.nalimov.tm.service.TaskService;
import ru.nalimov.tm.service.UserService;

import java.util.List;

public class TaskController extends  AbstractController {

    private final TaskService taskService;

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    private final UserService userService;

    private final Logger logger = LogManager.getLogger(TaskController.class);

    private static TaskController instance = null;

    public TaskController() {
        this.taskService = TaskService.getInstance();
        this.projectTaskService = ProjectTaskService.getInstance();
        this.userService = UserService.getInstance();
        this.projectService = ProjectService.getInstance();
    }

    public static TaskController getInstance() {
        synchronized (TaskController.class) {
            return instance == null
                    ? instance = new TaskController()
                    : instance;
        }
    }

    public int createTask() {
        System.out.println("[CREATE_TASK]");
        logger.info("[CREATE_TASK]");
        System.out.println("INPUT TASK NAME");
        final String name = scanner.nextLine();
        System.out.println("PLEASE ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        final Long userId = Application.userIdCurrent;
        taskService.create(name, description, userId);
        System.out.println("OK");
        return 0;
    }

    public int clearTask() {
        System.out.println("[CLEAR_TASK]");
        logger.info("[CLEAR_TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int viewTask(final Task task) {
        System.out.println("[VIEW TASK]");
        System.out.println("ID: "+task.getId());
        System.out.println("NAME: "+task.getName());
        System.out.println("DESCRIPTION: "+task.getDescription());
        System.out.println("[OK]");
        return 0;
    }

    public int viewTaskByIndex() throws TaskNotFoundException {
        System.out.println("ENTER, TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task;
        try {
            task = taskService.findByIndex(index);
            viewTask(task);
            return 0;
        } catch (TaskNotFoundException e) {
            logger.error(e);
            throw e;
        }
    }


    public int viewTaskById() throws TaskNotFoundException {
        System.out.println("ENTER, TASK INDEX:");
        final long id = scanner.nextLong();
        final Task task = taskService.findById(id);
        viewTask(task);
        return 0;
    }

    public int updateTaskByIndex() throws TaskNotFoundException {
        System.out.println("[UPDATE TASK]");
        logger.info("UPDATE TASK BY INDEX");
        System.out.println("PLEASE, ENTER TASK INDEX");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task;
        try {
            task = taskService.findByIndex(vId);
            System.out.println("PLEASE, ENTER TASK NAME");
            final String name = scanner.nextLine();
            System.out.println("PLEASE, ENTER TASK DESCRIPTION");
            final String description = scanner.nextLine();
            if (taskService.update(task.getId(), name, description) != null) {
                System.out.println("[OK]");
            } else {
                System.out.println("[FAIL]");
            }
        } catch (TaskNotFoundException e) {
            logger.error(e);
            throw e;
        }
        return 0;
    }


    public int updateTaskById() throws TaskNotFoundException {
        System.out.println("[UPDATE TASK]");
        logger.info("UPDATE TASK BY ID");
        System.out.println("PLEASE, ENTER TASK ID");
        final long id = Long.parseLong(scanner.nextLine());
        final Task task = taskService.findById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION");
        final String description = scanner.nextLine();
        taskService.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() throws TaskNotFoundException {
        System.out.println("[REMOVE TASK BY ID]");
        logger.info("REMOVE TASK BY ID");
        System.out.println("PLEASE, ENTER TASK ID");
        final long id = scanner.nextLong();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() throws TaskNotFoundException {
        System.out.println("[REMOVE TASK BY INDEX]");
        logger.info("REMOVE TASK BY INDEX");
        System.out.println("PLEASE, ENTER TASK INDEX");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }


    public int listTask() {
        System.out.println("[LIST_TASK]");
        int index = 1;
        viewTasks(taskService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        taskService.TaskSortByName(tasks);
        for (final Task task: taskService.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    public int listTaskByProjectId() {
        System.out.println("[LIST TASK TO PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToProjectByIds() throws TaskNotFoundException, ProjectNotFoundException {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        logger.info("ADD TASK TO PROJECT BY IDS");
        System.out.println("PLEASE, ENTER PROJECT ID");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[ADD TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskProjectByIds() throws TaskNotFoundException {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        logger.info("REMOVE TASK TO PROJECT BY IDS");
        System.out.println("PLEASE, ENTER PROJECT ID");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[REMOVE TASK FROM ID]");
        System.out.println("PLEASE, ENTER TASK ID");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }


    public int addTaskToUser() {
        System.out.println("ADDING TASK TO USER");
        logger.info("ADDING TASK TO USER");
        try {
            System.out.print("Enter task id: ");
            final Long taskId = Long.parseLong(scanner.nextLine());
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            final Task task = taskService.addTaskToUser(taskId, userId);
            System.out.println("Added user to task: " + task.toString());
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                addTaskToUser();
        }
        return 0;
    }

    public int clearTasksByUserId() {
        System.out.println("CLEARING TASKS BY USERID");
        logger.info("CLEARING TASKS BY USERID");
        try {
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            taskService.clearTasksByUserId(userId);
            System.out.println("Removed all tasks with user id " + userId + " ");
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                clearTasksByUserId();
        }
        return 0;
    }

    public int viewTaskListByUserId() {
        System.out.println(" VIEWING TASKS BY USERID ");
        logger.info(" VIEWING TASKS BY USERID ");
        try {
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            viewTasks(taskService.findAllByUserId(userId));
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                viewTaskListByUserId();
        }
        return 0;
    }

    public int viewTaskListByUserId(final Long userId) {
        System.out.println("VIEWING TASKS BY USERID " + userId + " ");
        viewTasks(taskService.findAllByUserId(userId));
        return 0;
    }

}
