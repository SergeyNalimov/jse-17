package ru.nalimov.tm;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nalimov.tm.controller.ProjectController;
import ru.nalimov.tm.controller.SystemController;
import ru.nalimov.tm.controller.TaskController;
import ru.nalimov.tm.controller.UserController;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.exceptions.TaskNotFoundException;
import ru.nalimov.tm.service.ProjectService;
import ru.nalimov.tm.service.ProjectTaskService;
import ru.nalimov.tm.service.TaskService;
import ru.nalimov.tm.service.UserService;

import java.util.Scanner;

import static ru.nalimov.tm.constant.TerminalConst.*;



/*Создали через Apache Maven + цикл while (с условием)*/

public class Application {

    private final ProjectController projectController = ProjectController.getInstance();

    private final UserController userController = UserController.getInstance();

    private final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    private final TaskController taskController = TaskController.getInstance();

    private final SystemController systemController = SystemController.getInstance();

    public static Long id = null;

    public static Long userIdCurrent = null;

    private static final Logger logger = LogManager.getLogger(Application.class.getName());

    {
        UserService.getInstance().create("TEST","pass1","Фам1","Имя1","Отч1");
        UserService.getInstance().create("ADMIN","pass2","Фам2","Имя2","Отч2",Role.ADMIN);
        ProjectService.getInstance().create("DEMO_PROJECT_3", "DESC PROJECT 3", UserService.getInstance().findByLogin("ADMIN").getId());
        ProjectService.getInstance().create("DEMO_PROJECT_2", "DESC PROJECT 2", UserService.getInstance().findByLogin("TEST").getId());
        TaskService.getInstance().create("TEST_TASK_2", "DESC TASK 2", UserService.getInstance().findByLogin("TEST").getId());
        TaskService.getInstance().create("TEST_TASK_1", "DESC TASK 1", UserService.getInstance().findByLogin("TEST").getId());

    }

    public static void main(final String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Application app = new Application();
        app.systemController.displayWelcome();
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            try {
                app.run(command);
            } catch (ProjectNotFoundException | TaskNotFoundException e) {
                //logger.error(e);
                e.getMessage();
            }
        }
    }

    private void run(final String[] args) throws TaskNotFoundException, ProjectNotFoundException  {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    private int run(final String param) throws TaskNotFoundException, ProjectNotFoundException  {
    if (param == null || param.isEmpty()) return -1;
        systemController.addCommandToHistory(param);
    switch (param) {
            case VERSION: return SystemController.getInstance().displayVersion();
            case ABOUT: return SystemController.getInstance().displayAbout();
            case HELP: return SystemController.getInstance().displayHelp();
            case EXIT: return SystemController.getInstance().displayExit();
            case COMMAND_HISTORY: return SystemController.getInstance().displayHistory();


            case PROJECT_CREATE: return ProjectController.getInstance().createProject();
            case PROJECT_CLEAR: return ProjectService.getInstance().clear();
            case PROJECT_LIST: return ProjectController.getInstance().listProject();
            case PROJECT_VIEW_BY_INDEX: return ProjectController.getInstance().viewProjectByIndex();
            case PROJECT_VIEW_BY_ID: return ProjectController.getInstance().viewProjectById();
       //     case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return ProjectController.getInstance().removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return ProjectController.getInstance().removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return ProjectController.getInstance().updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID: return ProjectController.getInstance().updateProjectById();

            case TASK_CREATE: return TaskController.getInstance().createTask();
            case TASK_CLEAR: return TaskController.getInstance().clearTask();
            case TASK_LIST: return TaskController.getInstance().listTask();
            case TASK_VIEW_BY_INDEX: return TaskController.getInstance().viewTaskByIndex();
            case TASK_VIEW_BY_ID: return TaskController.getInstance().viewTaskById();
            case TASK_REMOVE_BY_ID: return TaskController.getInstance().removeTaskById();
        //    case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_INDEX: return TaskController.getInstance().removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return TaskController.getInstance().updateTaskByIndex();
            case TASK_UPDATE_BY_ID: return TaskController.getInstance().updateTaskById();
            case TASK_ADD_PROJECT_BY_IDS: return TaskController.getInstance().addTaskToProjectByIds();
            case TASK_REMOVE_PROJECT_BY_IDS: return TaskController.getInstance().removeTaskProjectByIds();
            case TASK_LIST_BY_PROJECT_ID: return TaskController.getInstance().listTaskByProjectId();

            case TASK_ADD_TO_USER_BY_IDS: return TaskController.getInstance().addTaskToUser();
            case TASK_CLEAR_BY_USER_ID: return TaskController.getInstance().clearTasksByUserId();
            case TASK_LIST_BY_USER_ID: return TaskController.getInstance().viewTaskListByUserId();


            case USER_CREATE: return UserController.getInstance().createUser();
            case ADMIN_CREATE: return UserController.getInstance().createUser(Role.ADMIN);
            case USERS_CLEAR: return UserController.getInstance().clearUsers();
            case USERS_LIST: return UserController.getInstance().listUsers();
            case USER_VIEW_BY_LOGIN: return UserController.getInstance().viewUserByLogin();
            case USER_REMOVE_BY_LOGIN: return UserController.getInstance().removeUserByLogin();
            case USER_UPDATE_BY_LOGIN: return UserController.getInstance().updateUserByLogin();
            case USER_VIEW_BY_ID: return UserController.getInstance().viewUserById();
            case USER_REMOVE_BY_ID: return UserController.getInstance().removeUserById();
            case USER_UPDATE_BY_ID: return UserController.getInstance().updateUserById();
            case USER_AUTHENTIC: return UserController.getInstance().userAuthentic();
            case USER_DEAUTHENTIC: return UserController.getInstance().userDeauthentic();
            case USER_CHANGE_PASSWORD: return UserController.getInstance().userChangePassword();
            case USER_UPDATE_ROLE: return UserController.getInstance().updateRole();
            case USER_PROFILE_UPDATE: return UserController.getInstance().updateProfile(id);
            case USER_PROFILE_VIEW: return UserController.getInstance().userProfile(id);
            case PROJECT_ADD_TO_USER_BY_IDS: return ProjectController.getInstance().addProjectToUser();
            case PROJECT_CLEAR_BY_USER_ID: return ProjectController.getInstance().clearProjectsByUserId();
            case PROJECT_LIST_BY_USER_ID: return ProjectController.getInstance().viewProjectListByUserId();
        default: return systemController.displayErr();
        }
    }

}
