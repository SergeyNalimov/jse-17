package ru.nalimov.tm.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nalimov.tm.Application;
import ru.nalimov.tm.entity.Project;
import ru.nalimov.tm.exceptions.ProjectNotFoundException;
import ru.nalimov.tm.repository.ProjectRepository;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class ProjectService {

    private static final ProjectRepository projectRepository = ProjectRepository.getInstance();

 //   private static final TaskRepository taskRepository = TaskRepository.getInstance();

    private final static Logger logger = LogManager.getLogger(ProjectService.class);

    private static ProjectService instance = null;

    public ProjectService() {
    }

    public static ProjectService getInstance() {
        synchronized (ProjectService.class) {
            return instance == null
                    ? instance = new ProjectService()
                    : instance;
        }
    }

    public Project create(String name, String description, Long userid) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
       // logger.trace("CREATE PROJECT " + name + " " + description + " " + userid);
        return projectRepository.create(name, description, userid);
    }

    public Project create(final String name, final UUID userId) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("CREATE PROJECT " + name);
        return projectRepository.create(name, userId);
    }


   /* public int createProject(final String projectName) {
        projectRepository.create(projectName, null);
        System.out.println("TEST");
        return 0;
    }*/


    public Project update(Long id, String name, String description) throws ProjectNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        logger.trace("UPDATE PROJECT " + name + " " + description + " " + id);
        return projectRepository.update(id, name, description);
    }

    public Project findByIndex(int index) throws ProjectNotFoundException {
        if (index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    public Project findById(Long id) throws ProjectNotFoundException {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    public Project removeById(Long id) throws ProjectNotFoundException {
        if (id == null) return null;
        logger.trace("REMOVE PROJECT BY ID " + id);
        return projectRepository.removeById(id);
    }

    public Project removeByIndex(int index) throws ProjectNotFoundException {
        if (index < 0 || index > projectRepository.size() -1) return null;
        if (projectRepository.findByIndex(index) == null) return null;
        if (projectRepository.findByIndex(index).getUserId().equals(Application.userIdCurrent)) return projectRepository.removeByIndex(index);
        else return null;

    }

    public int clear() {
        projectRepository.clear();
        return 0;
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project addProjectToUser(final Long projectId, final Long userId) throws ProjectNotFoundException {
        final Project project = projectRepository.findById(projectId);
        if (project == null)
            return null;
        project.setUserId(userId);
        return project;
    }

    public void clearProjectsByUserId(Long userId) throws ProjectNotFoundException {
        projectRepository.clearProjectsByUserId(userId);
    }

    public List<Project> findAllByUserId(Long userId) {
        return projectRepository.findAllByUserId(userId);
    }

    public void ProjectSortByName(List<Project> projects) {
        Collections.sort(projects, Project.ProjectSortByName);
    }

}
