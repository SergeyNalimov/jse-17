package ru.nalimov.tm.service;

import ru.nalimov.tm.entity.User;
import ru.nalimov.tm.repository.UserRepository;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.util.HashMD5;

import java.util.*;

public class UserService {

    private static final UserRepository userRepository = UserRepository.getInstance();

    private final List<User> users = new ArrayList<>();

    private Long userAuthenticId;
    private Long userDeauthentic;

    private static UserService instance = null;

    public UserService() {
    }

    public UserService(UserRepository userRepository) {

    }

    public static UserService getInstance() {
        synchronized (UserService.class) {
            return instance == null
                    ? instance = new UserService()
                    : instance;
        }
    }

    public User create(
            final String login, final String userPassword, final String firstName,
            final String secondName, final String lastName
    ){
        final User user = new User(login, userPassword, firstName, secondName, lastName);
        users.add(user);
        return user;
    }

    public User create(
            final String login, final String userPassword, final String firstName, final String secondName,
            final String lastName, Role userRole
    ){
        final User user = new User(login, userPassword, firstName, secondName, lastName, userRole);
        users.add(user);
        return user;
    }

    public User findByLogin(final String login) {
        for (final User user: users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateByLogin(
            final String login, final String userPassword, final String firstName, final String secondName,
            final String lastName
    ){
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setLastName(lastName);
        return user;
    }

    public User findById(final Long id) {
        if (id == null) return null;
        for (final User user: users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    public User removeById(final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateById(
            final Long id, final String login, final String userPassword, final String firstName,
            final String secondName, final String lastName
    ) {
        final User user = findById(id);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setLastName(lastName);
        return user;
    }

    public void clear(){
        users.clear();
    }

    public int getSize() {
        return users.size();
    }

    public List<User> findAll()  {
        return users;
    }

    public User userAuthentic(String login, String userPassword) {
        User user = findByLogin(login);
        if (user == null) return null;
        if (!user.getHashPassword().equals(HashMD5.getHash(userPassword))) return null;
        this.setUserAuthenticId(user.getId());
        return user;
    }

    public void setUserAuthenticId(Long userAuthenticId) {
        this.userAuthenticId = userAuthenticId;
    }

    public void userDeauthentic(){
        this.userDeauthentic = null;
    }

    public User userChangePassword(String login, String userOldPassword, String userNewPassword) {
        User user = findByLogin(login);
        if (user == null) return null;
        if (!user.getHashPassword().equals(HashMD5.getHash(userOldPassword))) return null;
        user.setHashPassword(userNewPassword);
        return user;
    }

    public User updateProfile(Long id, String login, String firstname, String secondname, String lastname) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.updateProfile(id, login, firstname, secondname, lastname);
    }

    public User updateRole(String login, String role) {
        if (login == null || login.isEmpty()) return null;
        if (role == null || role.isEmpty()) return null;
        return userRepository.updateRole(login, role);
    }

}
