package ru.nalimov.tm.exceptions;

public class TaskNotFoundException extends Exception{
    public TaskNotFoundException(String errorMsg) {
        super(errorMsg);
    }
}
