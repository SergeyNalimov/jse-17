package ru.nalimov.tm.exceptions;

public class ProjectNotFoundException extends Exception {
    public ProjectNotFoundException(String errorMsg) {
        super(errorMsg);
    }
}
